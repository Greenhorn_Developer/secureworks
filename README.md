# FriendsApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

To see code coverage, download the application, run NPM install from the root of the application folder, and then run `npm run test`.
The code coverage report will be seen in the test result, or you can see the `coverage\friendsApp\` folder for the `index.html` file with the 
coverage results.

# Live Demo

You can see a live demo of this application hosted [here](https://upbeat-nobel-c24b2b.netlify.app/)