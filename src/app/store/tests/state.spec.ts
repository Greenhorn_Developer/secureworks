import {friendReducer, initialState} from '../friend.reducer'
import * as fromActions from '../friend.actions'
import { v4 as uuidv4 } from 'uuid';
import {Friend} from '../../friends/models/friend'
import * as fromSelectors from '../friend.selector'

describe('Friends State', () => {

  describe('Friends Reducer', ()=>{
    it('should return default state', ()=>{
        const action = {type: 'Blank'}
        const state = friendReducer(initialState, action)
        expect(state).toEqual(initialState);
      })

    it('should return initial state from LOAD_ALL_FRIENDS', ()=>{
      const action = fromActions.LOAD_ALL_FRIENDS
      const state = friendReducer(initialState, action)
      expect(state).toEqual(initialState);
    })

    it('should return newly created state from CREATE_FRIEND', ()=>{
      const newFriend: Friend = {friendId: uuidv4(), name: 'George',age: 32,weight: 150,friendList: ['Roger']}
      const allFriends = []
      allFriends.push(newFriend)
      const action = fromActions.CREATE_FRIEND({friend: newFriend})
      const state = friendReducer(initialState, action)
      expect(state.allFriends).toEqual(allFriends)
    })
  })

  describe('Friends Actions', () => {

    it('should create LOAD_ALL_FRIENDS action', ()=>{
      const action = fromActions.LOAD_ALL_FRIENDS()
      expect({...action}).toEqual({
        type: '[Friend Dashboard] View All Friends'
      })
    })

    it('should create CREATE_FRIEND action', ()=>{
      const friend = {friendId: uuidv4(), name: 'George',age: 32,weight: 150,friendList: ['Roger']}
      const action = fromActions.CREATE_FRIEND({friend})
      expect({...action}).toEqual({
        type: '[Friend Form] Create Friend',
        friend
      })
    })
  })

  describe('Friends Selectors', ()=>{

    it('should get an empty initial state', () => {
      const result = fromSelectors.getFriendState.projector(initialState)
      expect(result).toBe(undefined)
    });

    it('should get an empty initial state for combined friends', () => {
      const result = fromSelectors.getCombinedFriends.projector(initialState)
      expect(result.length).toEqual(0)
    });
  

  })


});