import { Friend } from '../../app/friends/models/friend'
import * as fromFriends from '../store/friend.actions'
import { Action, createReducer, on } from '@ngrx/store';

export const featureKey = 'FriendsDashboard'

export interface AppState {
  individualFriend: Friend,
  allFriends: Friend[]
}

export const initialState : AppState ={
  individualFriend:{
    friendId: '',
    name:'',
    age: null,
    weight: null,
    friendList:[]
  },
  allFriends: [],
}


const reducerFunc = createReducer(
  initialState,
  on(fromFriends.LOAD_ALL_FRIENDS, state => ({...state})),
  on(fromFriends.CREATE_FRIEND, (state, action)=> {
    const allFriends = [...state.allFriends]
    allFriends.push(action.friend)
    return { ...state, allFriends}
  })
)

export function friendReducer(state: AppState, action :Action){
  return reducerFunc(state, action)
}

// get slices of state for selectors
export const getAllFriends = (state:AppState) => state?.allFriends

