import {Friend} from "../../app/friends/models/friend"

export interface AppState {
    friendId: string,
    friend: Friend,
    allFriends: []
    loading: boolean,
    loaded: boolean
  
  }
  
  export const appstate: AppState = {} as AppState;


