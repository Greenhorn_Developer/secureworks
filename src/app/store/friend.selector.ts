import { createFeatureSelector, createSelector } from '@ngrx/store'
import { FriendAppState } from '../reducers/index'
import { featureKey, getAllFriends } from '../store/friend.reducer'


//using feature key, get all state for that feature module
export const getFriendAppState = createFeatureSelector<FriendAppState>(featureKey)

export const getFriendState = createSelector(
    getFriendAppState,
    (state: FriendAppState)=> state?.friendList
)

export const getCombinedFriends = createSelector(getFriendState, getAllFriends)



