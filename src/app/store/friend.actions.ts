import { createAction, props } from '@ngrx/store'
import {  Friend } from '../friends/models/friend'

/* View the friends */
export const LOAD_ALL_FRIENDS = createAction('[Friend Dashboard] View All Friends')

/* Create new friends */
export const CREATE_FRIEND = createAction('[Friend Form] Create Friend', props<{friend: Friend}>())


