export interface Friend{
    friendId: string;
    name: string;
    friendList: Array<string>
    age: number;
    weight: number;

}

