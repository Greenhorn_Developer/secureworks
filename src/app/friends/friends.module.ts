//angular imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

/* vendor imports */
//ngrx modules
import { StoreModule } from '@ngrx/store';

//angular material
import { MatTableModule } from '@angular/material/table'
import { MatInputModule } from '@angular/material/input'; 
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips'; 
import { MatAutocompleteModule } from '@angular/material/autocomplete'; 
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
//custom imports
import { featureKey } from '../store/friend.reducer'
import { reducers } from '../store'
import { FriendFormComponent } from './components/friend-form/friend-form.component';
import { FriendDashboardComponent } from './containers/friend-dashboard/friend-dashboard.component';
import { ChartContainerComponent } from './containers/chart-container/chart-container.component';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';

@NgModule({
  declarations: [
    FriendFormComponent,
    FriendDashboardComponent,
    ChartContainerComponent,
    BarChartComponent,
  ],
  imports: [
    //angular imports
    CommonModule,
    ReactiveFormsModule,
    //vendor imports
    MatTableModule,
    MatInputModule,
    MatFormFieldModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatIconModule,
    MatCardModule,
    StoreModule.forFeature(featureKey, reducers),

  ],
  exports:[
    FriendDashboardComponent
  ]
})
export class FriendsModule { }
