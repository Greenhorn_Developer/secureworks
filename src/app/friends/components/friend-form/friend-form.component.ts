//angular imports
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms'

/* vendor imports */
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { Store } from '@ngrx/store';
import { v4 as uuidv4 } from 'uuid';

//custom imports
import { CREATE_FRIEND} from '../../../store/friend.actions'
import { FriendAppState } from '../../../reducers/index'


@Component({
  selector: 'app-friend-form',
  templateUrl: './friend-form.component.html',
  styleUrls: ['./friend-form.component.css']
})
export class FriendFormComponent implements OnInit{

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  friendForm = new FormGroup({
    name: new FormControl('',[Validators.required, Validators.minLength(3)]),
    age: new FormControl('',[Validators.required, Validators.pattern(/^[0-9]*$/ )]),
    weight: new FormControl('',[Validators.required, Validators.pattern(/^[0-9]*$/ )]),
    friendList: new FormControl('',[Validators.required])
  })

  constructor(private store: Store<FriendAppState>) { }

  ngOnInit(){}

  get friendList() {
    return this.friendForm.get('friendList');
  }

  /* Called on the Material Chip Component when Friend is added */
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add the friend to the form
    if ((value || '').trim()) {
      this.friendList.setValue([...this.friendList.value, value.trim()]);
      this.friendList.updateValueAndValidity();
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(friend: string): void {
    const index = this.friendList.value.indexOf(friend);

    if (index >= 0) {
      this.friendList.value.splice(index, 1);
      this.friendList.updateValueAndValidity();
    }
  }

  onSubmit(){

    let friend = {
      friendId: uuidv4(),
      name: this.friendForm.controls.name.value,
      age: parseInt(this.friendForm.controls.age.value),
      weight: parseInt(this.friendForm.controls.weight.value),
      friendList: this.friendForm.controls.friendList.value
    }
    
    this.store.dispatch(CREATE_FRIEND({friend}))
    this.friendForm.reset()
    this.friendForm.patchValue({friendList: []})
  
  }


}
