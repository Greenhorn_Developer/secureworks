import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { FriendFormComponent } from './friend-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input'; 
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips'; 
import { MatAutocompleteModule } from '@angular/material/autocomplete'; 
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { CREATE_FRIEND } from '../../../store/friend.actions'

describe('Friend Form Component', () => {
  let component: FriendFormComponent;
  let fixture: ComponentFixture<FriendFormComponent>;
  let name;
  let age;
  let weight;
  let friendList;
  let store: MockStore;
  let dispatchSpy;

  const initialState = {
    individualFriend:{
      friendId: '',
      name:'',
      age: null,
      weight: null,
      friendList:[]
    },
    allFriends: [{
      friendId: '23423-21dwd',
      name:'Tommy',
      age: 30,
      weight: 123,
      friendList:['Frankie']
    }],
  }
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FriendFormComponent ],
      imports:[
        BrowserAnimationsModule,
        MatInputModule,
        MatFormFieldModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatIconModule,
        MatCardModule,
        ReactiveFormsModule
      ],
      providers: [provideMockStore({initialState})],
    })
    .compileComponents();
    store = TestBed.inject(MockStore);
    store.setState(initialState)
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendFormComponent);
    component = fixture.componentInstance;
    name = component.friendForm.controls.name;
    age = component.friendForm.controls.age;
    weight = component.friendForm.controls.weight;
    friendList = component.friendForm.controls.friendList;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Friend Form', () =>{

    it('should initialize with a blank form', ()=> {
      expect(component.friendForm.valid).toBeFalsy();
    })

    it('should validate the empty form fields', ()=>{

      name.setValue('')
      age.setValue(null)
      weight.setValue(null)
      friendList.setValue([])

      expect(name.hasError('required')).toBeTruthy();
      expect(age.hasError('required')).toBeTruthy();
      expect(weight.hasError('required')).toBeTruthy();
      expect(friendList.hasError('required')).toBeTruthy();
    })

    it('should add two friends to the friend list', ()=>{
      //input is null as we are not testing if it is an HTMLInputElement
      component.add({input:null, value: 'Frank'})
      expect(friendList.valid).toBeTruthy()
      expect(friendList.value.length).toEqual(1)

      //add friend with leading space
      component.add({input:null, value: ' Joe'})
      expect(friendList.valid).toBeTruthy()
      expect(friendList.value.length).toEqual(2)
    })
    
    it('should not add an empty friend', ()=>{
      component.add({input:null, value: undefined})
      expect(friendList.valid).toBeFalsy()
      expect(friendList.value.length).toEqual(0)
    })

    it('should remove a friend' ,()=>{
      component.add({input:null, value: 'Frank'})
      component.remove('Frank')
      expect(friendList.valid).toBeFalsy()
      expect(friendList.value.length).toEqual(0)
    })

    it('should submit the friend form and dispatch an action', ()=>{

      dispatchSpy = spyOn(store, 'dispatch')

      name.setValue('Roger')
      age.setValue(12)
      weight.setValue(12)
      friendList.setValue(['Thomase'])

      let friend = {
        friendId: '23233-2323-3wdds',
        name: component.friendForm.controls.name.value,
        age: parseInt(component.friendForm.controls.age.value),
        weight: parseInt(component.friendForm.controls.weight.value),
        friendList: component.friendForm.controls.friendList.value
      }

      component.onSubmit()
      
      store.dispatch(CREATE_FRIEND({friend}))
      expect(dispatchSpy).toHaveBeenCalledTimes(2)
      expect(dispatchSpy).toHaveBeenCalledWith({ type: '[Friend Form] Create Friend', friend})

      //clear out the friendlist
      expect(friendList.valid).toBeFalsy()
      expect(friendList.value.length).toEqual(0)
    })
    
  })



});
