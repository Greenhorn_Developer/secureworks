//angular imports
import { AfterContentInit, ChangeDetectionStrategy, Component, Input,  } from '@angular/core';

//vendor imports
import * as d3 from 'd3';

//custom imports
import { FilteredData } from '../../models/filteredData';

@Component({
  selector: 'app-bar-chart',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements  AfterContentInit {

  @Input()filteredData: FilteredData[];
  svg;
  bars;
  max;
  xScale;
  yScale;
  yAxis;
  xAxis;
  bottomLabel;
  
  //D3 Setup for Bar Chart
  margin = {
    top: 40,
    right: 40,
    bottom: 40,
    left: 40
  }

  width = 700 - this.margin.left - this.margin.right;
  height = 800 - this.margin.top - this.margin.bottom

  //draw chart after component is ready
  ngAfterContentInit() {
    if(this.filteredData){
      this.createSVG();
      this.drawBars()
    }
  }

  ngOnChanges(){
    if(this.svg){
      this.drawBars()
    }
  }

  createSVG(){
    this.svg = d3.select('.bar-chart-container')
      .append('svg')
      .attr('width', this.width + this.margin.left + this.margin.right)
      .attr('height', this.height + this.margin.top + this.margin.bottom)
      .append('g')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)
       
    this.max = d3.max(this.filteredData, (data:FilteredData) => data.count)

    //range - pixels on page
    this.xScale = d3.scaleBand().rangeRound([0, this.width])
    this.yScale = d3.scaleLinear().range([this.height, 0]);
  
    this.xAxis = d3.axisBottom(this.xScale).tickFormat((data:any)=> `Age ${data}`);
    this.yAxis = d3.axisLeft(this.yScale);
  
    //axis bottom will show up at the top unless positioned with an offset (transform/translate)
    this.svg
      .append("g")
      .attr("class", "xAxis")
      .attr("transform", "translate(0," + this.height + ")")
      .selectAll("text")
      .call(this.xAxis);
  
    this.svg
      .append("g")
      .attr("class", "yAxis")
      .call(this.yAxis);
  }
  
  drawBars() {
    this.max = d3.max(this.filteredData, (data: FilteredData) => data.count);
     
    //domain - values of data
    this.xScale.domain(this.filteredData.map((data: FilteredData) => data.age)).paddingInner(0.05);
    this.yScale.domain([0, parseInt(this.max)]);
  
    this.svg.select(".yAxis")
      .transition()
      .duration(500)
      .call(this.yAxis);
    
    this.svg
      .select(".xAxis")
      .attr("transform", "translate(0," + this.height + ")")
      .transition()
      .duration(500)
      .call(this.xAxis);

    this.svg
      .selectAll("rect")
      .data(this.filteredData)
      .join(
        enter => {
          enter
            .append("rect")
            .style("fill", "#FF8B8A")
            //position on graph the count values
            .attr("y", (d: FilteredData) => this.yScale(d.count))
            //position on graph the age values
            .attr("x", (d: FilteredData) => this.xScale(d.age))
            //calculate the width of the bar
            .attr("width", this.xScale.bandwidth())
            //calculate the difference between the height of the visualization and the position of the bar on y axis
            .attr("height", (d: FilteredData) => this.height - this.yScale(d.count))
            .transition()
            .duration(500)
            .style("fill", "#943332");
        },
        update => {
          update
            .attr("y", (d: FilteredData) => this.yScale(d.count))
            .attr("x", (d: FilteredData) => this.xScale(d.age))
            .attr("width", this.xScale.bandwidth())
            .attr("height", (d: FilteredData) => this.height - this.yScale(d.count))
            .transition()
            .duration(500)
            .style("fill", "#943332"); 
                         
        },
        exit => {
          exit.remove();
        }
      );
  }

}
