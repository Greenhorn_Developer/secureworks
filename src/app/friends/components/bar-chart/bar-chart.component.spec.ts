import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BarChartComponent } from './bar-chart.component';
import * as d3 from 'd3';
import { F } from '@angular/cdk/keycodes';
import { FileDetector } from 'protractor';
import { filter } from 'd3';
import { FilteredData } from '../../models/filteredData';

describe('BarChartComponent', () => {
  let component: BarChartComponent;
  let fixture: ComponentFixture<BarChartComponent>;
  let svg;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarChartComponent ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    svg = d3.select('.bar-chart-container')
  });

  afterEach(function() {
    svg.remove();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an empty chart', ()=>{
    component.filteredData = [];
    component.ngAfterContentInit()
    expect(svg.selectAll('.yAxis').empty()).toBeFalsy();
    expect(svg.selectAll('.xAxis').empty()).toBeFalsy();
    expect(svg.selectAll('rect').size()).toEqual(0);
  })

  it('should create a bar when data is present', ()=>{
    component.filteredData = [{age: 20, count: 1}]
    component.ngAfterContentInit()
    expect(svg.selectAll('rect').size()).toEqual(1);
  })

  it('should create two bars when data is present', ()=>{
    component.filteredData = [{age: 20, count: 1}, {age: 22, count: 1} ]
    component.ngAfterContentInit()
    expect(svg.selectAll('rect').size()).toEqual(2);
  })

  it('should not draw bars when data is missing', ()=>{
    spyOn(component, 'drawBars')
    component.filteredData = undefined
    expect(svg.selectAll('rect').size()).toEqual(0);
    component.ngAfterContentInit()
    expect(component.drawBars).not.toHaveBeenCalled();
  })

});
