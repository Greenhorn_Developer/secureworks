import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';

import { ChartContainerComponent } from '../chart-container/chart-container.component';
import { FriendFormComponent } from '../../components/friend-form/friend-form.component';
import { FriendDashboardComponent } from './friend-dashboard.component';

import { featureKey } from '../../../store/friend.reducer'
import { reducers } from '../../../store'
import { ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { BarChartComponent } from '../../components/bar-chart/bar-chart.component';

describe('Friend Dashboard Component', () => {
  let component: FriendDashboardComponent;
  let fixture: ComponentFixture<FriendDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        FriendFormComponent,
        FriendDashboardComponent,
        ChartContainerComponent,
        BarChartComponent
      ],
        imports:[
          BrowserAnimationsModule,
          MatTableModule,
          MatInputModule,
          MatFormFieldModule,
          MatChipsModule,
          MatAutocompleteModule,
          MatIconModule,
          MatCardModule,
          ReactiveFormsModule,
          StoreModule.forRoot({}), 
          StoreModule.forFeature(featureKey, reducers),
        ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
