//angular imports
import {  Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

/* vendor imports */
import { Observable } from 'rxjs';
import * as d3 from 'd3';

//custom imports
import { Friend } from '../../models/friend';

//store imports
import { FriendAppState } from '../../../reducers/index'
import { getCombinedFriends } from '../../../store/friend.selector'
import { LOAD_ALL_FRIENDS } from '../../../store/friend.actions'
import { FilteredData } from '../../models/filteredData';

@Component({
  selector: 'app-chart-container',
  templateUrl: './chart-container.component.html',
  styleUrls: ['./chart-container.component.css']
})
export class ChartContainerComponent implements OnInit {

  storeData$ : Observable<Friend[]> = this.store.select(getCombinedFriends)
  filteredData : FilteredData[];

  ngOnInit(){
    this.store.dispatch(LOAD_ALL_FRIENDS())
    this.storeData$.subscribe( data => {
      if(data){
        this.filteredData = this.prepareData(data)
      } else{
        this.filteredData = []
      }
    })
  }

  constructor(private store: Store<FriendAppState>) { }
  
  prepareData(arr){
    // filter
    const filteredData = arr.filter(friend => friend.age < 100 && friend.age > 0)
    //create map of grouped data
    const dataMap = d3.rollup(filteredData, v => v.length, (d:Friend) => d.age)
    //convert map to array, group age by total count of age
    const dataArray = Array.from( dataMap, d=> ({ age: d[0], count: d[1]}))

    //sort by ascending age
    dataArray.sort((a,b)=> d3.ascending(a.age,b.age))
    
    return dataArray;
  }


}
