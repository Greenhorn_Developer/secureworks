import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { BarChartComponent } from '../../components/bar-chart/bar-chart.component';
import { ChartContainerComponent } from './chart-container.component';

describe('Friend Chart Component', () => {
  let component: ChartContainerComponent;
  let fixture: ComponentFixture<ChartContainerComponent>;
  let store: MockStore;

  const initialState = {
    individualFriend:{
      friendId: '',
      name:'',
      age: null,
      weight: null,
      friendList:[]
    },
    allFriends: []
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartContainerComponent, BarChartComponent ],
      providers: [provideMockStore({initialState})],
    })
    .compileComponents();
    store = TestBed.inject(MockStore);
    store.setState(initialState)
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not have data after initialized', ()=>{
    expect(component.filteredData).toEqual([])
  })

  it('should prepare data for the bar chart', ()=>{
    let data = [
      { friendId: '1', name:'Roger', age: 20, weight: 120, friendList:[] },
      { friendId: '2', name:'Frank', age: 30, weight: 150, friendList:[] },
      { friendId: '3', name:'Alph', age: 160, weight: 231, friendList:[] }
    ]

    //we are not expecting anything over the age of 100 to show up due to the filter in prepareData
    let expectedPreparedData = [{ age: 20, count: 1}, {age: 30, count:1}]
    let unsortedPreparedData = [{ age: 30, count: 1}, {age: 20, count:1}]

    let preparedData = component.prepareData(data)
    //should only have age and count keys
    expect(preparedData).toEqual(expectedPreparedData)
    //will fail because data isn't sorted
    expect(preparedData).not.toEqual(unsortedPreparedData)
  })
  
});
