import * as fromFriends from '../store/friend.reducer'
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store'

export interface FriendAppState{
    friendList: fromFriends.AppState
}

//type check reducers with action reducer map
export const reducers: ActionReducerMap<FriendAppState> = {
    friendList: fromFriends.friendReducer
}

